/**********************************
 * FILE NAME: MP1Node.cpp
 *
 * DESCRIPTION: Membership protocol run by this Node.
 * 				Definition of MP1Node class functions.
 **********************************/

#include "MP1Node.h"

void serializeMemberMessage(MemberPkg &memberPkg, char **, int *);
MemberPkg *parseMemberMessage(char *msg, int len);
void verbosePkg(MemberPkg &memberPkg);

#define VERBOSE 0
/*
 * Note: You can change/add any functions in MP1Node.{h,cpp}
 */

/**
 * Overloaded Constructor of the MP1Node class
 * You can add new members to the class if you think it
 * is necessary for your logic to work
 */
MP1Node::MP1Node(Member *member, Params *params, EmulNet *emul, Log *log, Address *address) {
	for( int i = 0; i < 6; i++ ) {
		NULLADDR[i] = 0;
	}
	this->memberNode = member;
	this->emulNet = emul;
	this->log = log;
	this->par = params;
	this->memberNode->addr = *address;
}

/**
 * Destructor of the MP1Node class
 */
MP1Node::~MP1Node() {}

/**
 * FUNCTION NAME: recvLoop
 *
 * DESCRIPTION: This function receives message from the network and pushes into the queue
 * 				This function is called by a node to receive messages currently waiting for it
 */
int MP1Node::recvLoop() {
    if ( memberNode->bFailed ) {
    	return false;
    }
    else {
    	return emulNet->ENrecv(&(memberNode->addr), enqueueWrapper, NULL, 1, &(memberNode->mp1q));
    }
}

/**
 * FUNCTION NAME: enqueueWrapper
 *
 * DESCRIPTION: Enqueue the message from Emulnet into the queue
 */
int MP1Node::enqueueWrapper(void *env, char *buff, int size) {
	Queue q;
	return q.enqueue((queue<q_elt> *)env, (void *)buff, size);
}

/**
 * FUNCTION NAME: nodeStart
 *
 * DESCRIPTION: This function bootstraps the node
 * 				All initializations routines for a member.
 * 				Called by the application layer.
 */
void MP1Node::nodeStart(char *servaddrstr, short servport) {
    Address joinaddr;
    joinaddr = getJoinAddress();

    // Self booting routines
    if( initThisNode(&joinaddr) == -1 ) {
#ifdef DEBUGLOG
        log->LOG(&memberNode->addr, "init_thisnode failed. Exit.");
#endif
        exit(1);
    }

    if( !introduceSelfToGroup(&joinaddr) ) {
        finishUpThisNode();
#ifdef DEBUGLOG
        log->LOG(&memberNode->addr, "Unable to join self to group. Exiting.");
#endif
        exit(1);
    }

    return;
}

/**
 * FUNCTION NAME: initThisNode
 *
 * DESCRIPTION: Find out who I am and start up
 */
int MP1Node::initThisNode(Address *joinaddr) {
	/*
	 * This function is partially implemented and may require changes
	 */
	int id = *(int*)(&memberNode->addr.addr);
	int port = *(short*)(&memberNode->addr.addr[4]);

	memberNode->bFailed = false;
	memberNode->inited = true;
	memberNode->inGroup = false;
    // node is up!
	memberNode->nnb = 0;
	memberNode->heartbeat = 0;
	memberNode->pingCounter = TFAIL;
	memberNode->timeOutCounter = -1;
    initMemberListTable(memberNode);

    return 0;
}

/**
 * FUNCTION NAME: introduceSelfToGroup
 *
 * DESCRIPTION: Join the distributed system
 */
int MP1Node::introduceSelfToGroup(Address *joinaddr) {
    char *msg;
    int len;
#ifdef DEBUGLOG
    static char s[1024];
#endif

    if ( 0 == memcmp((char *)&(memberNode->addr.addr), (char *)&(joinaddr->addr), sizeof(memberNode->addr.addr))) {
        // I am the group booter (first process to join the group). Boot up the group
#ifdef DEBUGLOG
        log->LOG(&memberNode->addr, "Starting up group...");
#endif
        memberNode->inGroup = true;
    }
    else {
        // generate package
        MemberPkg *memberPkg;
        memberPkg = generateMemberPkg(JOINREQ);
        serializeMemberMessage(*memberPkg, &msg, &len);

        free(memberPkg);

#ifdef DEBUGLOG
        sprintf(s, "Trying to join...");
        log->LOG(&memberNode->addr, s);
#endif

        // send JOINREQ message to introducer member
        emulNet->ENsend(&memberNode->addr, joinaddr, (char *)msg, len);

        free(msg);
    }

    return 1;

}

/**
 * FUNCTION NAME: finishUpThisNode
 *
 * DESCRIPTION: Wind up this node and clean up state
 */
int MP1Node::finishUpThisNode(){
   /*
    * Your code goes here
    */
}

/**
 * FUNCTION NAME: nodeLoop
 *
 * DESCRIPTION: Executed periodically at each member
 * 				Check your messages in queue and perform membership protocol duties
 */
void MP1Node::nodeLoop() {
    if (memberNode->bFailed) {
    	return;
    }

    // Check my messages
    checkMessages();

    // Wait until you're in the group...
    if( !memberNode->inGroup ) {
    	return;
    }

    // ...then jump in and share your responsibilites!
    nodeLoopOps();

    return;
}

/**
 * FUNCTION NAME: checkMessages
 *
 * DESCRIPTION: Check messages in the queue and call the respective message handler
 */
void MP1Node::checkMessages() {
    void *ptr;
    int size;

    // Pop waiting messages from memberNode's mp1q
    while ( !memberNode->mp1q.empty() ) {
    	ptr = memberNode->mp1q.front().elt;
    	size = memberNode->mp1q.front().size;
    	memberNode->mp1q.pop();
    	recvCallBack((void *)memberNode, (char *)ptr, size);
    }
    return;
}

/**
 * FUNCTION NAME: recvCallBack
 *
 * DESCRIPTION: Message handler for different message types
 */
bool MP1Node::recvCallBack(void *env, char *data, int size ) {
	/*
	 * Your code goes here
	 */
    MemberPkg *recvPkg;
    recvPkg = parseMemberMessage(data, size);

    // printAddress(&((Member*)env)->addr);
    // printf("recvCallBack with :\t");
    // verbosePkg(*recvPkg);

    // if this is JOINREP/JOINREQ/GOSSIP, adding this node to list
    vector<MemberInfo>::size_type sz = recvPkg->memInfo.size();
    for (unsigned j=0; j<sz; j++)
        updateMemberList(&(recvPkg->memInfo[j]));

    verboseMemberList();

    // if this is JOINREP, add this node is in group
    if (recvPkg->msgType == JOINREP) {
        memberNode->inGroup = true;
    }

    // if this is JOINREQ, then generate response package
    if (recvPkg->msgType == JOINREQ) {
        // generate response message
        char *msg;
        int len;
        MemberPkg *responsePkg;

        responsePkg = generateMemberPkg(JOINREP);
        serializeMemberMessage(*responsePkg, &msg, &len);
        free(responsePkg);

        // sender address
        Address *requestAddr;
        requestAddr = &(recvPkg->memInfo.begin()->addr);
        // then reply
        emulNet->ENsend(&memberNode->addr, requestAddr, (char *)msg, len);
        free(msg);
    }

    free(recvPkg);

    return 0;
}

/**
 * FUNCTION NAME: nodeLoopOps
 *
 * DESCRIPTION: Check if any node hasn't responded within a timeout period and then delete
 * 				the nodes
 * 				Propagate your membership list
 */
void MP1Node::nodeLoopOps() {
    // check membership status
    // 
    // 
    // update this node heartbeat
    // 
    // then propagate membership list with random neighbors
    if( !memberNode->inGroup ) {
        return;
    }

    vector<MemberListEntry>::size_type sz = memberNode->memberList.size();

    // check membership status, flush outdated member
    vector<MemberListEntry>::iterator i;
    for (i=memberNode->memberList.begin(); i!= memberNode->memberList.end(); i++) {
        if ((par->getcurrtime() - i->timestamp > MEMBER_TIMEOUT) && (i->bFailed == false)) {
            i->bFailed = true;
            // and loging
            Address addr;
            *(int*)(&addr.addr) = i->getid();;
            *(short*)(&addr.addr[4]) = i->getport();
            log->logNodeRemove(&memberNode->addr, &addr);
        }
    }

    // update node heartbeat
    memberNode->heartbeat++;

    char *msg;
    int len;
    MemberPkg *responsePkg;
    
    responsePkg = generateMemberPkg(GOSSIP);
    serializeMemberMessage(*responsePkg, &msg, &len);
    free(responsePkg);

    // finally chose random neighbors to propagate
    // send up to 2 neighbors
    for (unsigned i=0; (i<sz || i<2); i++) {
        unsigned ranPos = rand()%sz;
        MemberListEntry mem = memberNode->memberList[ranPos];
        Address desAddr;
        *(int*)(&desAddr.addr) = mem.getid();;
        *(short*)(&desAddr.addr[4]) = mem.getport();
        // printf("Will send to ");
        // printAddress(&desAddr);
        emulNet->ENsend(&memberNode->addr, &desAddr, (char *)msg, len);
    }
    free(msg);

    return;
}

/**
 * FUNCTION NAME: isNullAddress
 *
 * DESCRIPTION: Function checks if the address is NULL
 */
int MP1Node::isNullAddress(Address *addr) {
	return (memcmp(addr->addr, NULLADDR, 6) == 0 ? 1 : 0);
}

/**
 * FUNCTION NAME: getJoinAddress
 *
 * DESCRIPTION: Returns the Address of the coordinator
 */
Address MP1Node::getJoinAddress() {
    Address joinaddr;

    memset(&joinaddr, 0, sizeof(Address));
    *(int *)(&joinaddr.addr) = 1;
    *(short *)(&joinaddr.addr[4]) = 0;

    return joinaddr;
}

/**
 * FUNCTION NAME: initMemberListTable
 *
 * DESCRIPTION: Initialize the membership list
 */
void MP1Node::initMemberListTable(Member *memberNode) {
	memberNode->memberList.clear();
}

/**
 * FUNCTION NAME: printAddress
 *
 * DESCRIPTION: Print the Address
 */
void MP1Node::printAddress(Address *addr)
{
    printf("%d.%d.%d.%d:%d \n",  addr->addr[0],addr->addr[1],addr->addr[2],
                                                       addr->addr[3], *(short*)&addr->addr[4]) ;    
}

MemberPkg *MP1Node::generateMemberPkg(MsgTypes type)
{
    MemberPkg *memberPkg = new MemberPkg;

    MemberInfo memInfo;
    vector<MemberListEntry>::iterator i;
    for (i=memberNode->memberList.begin(); i!= memberNode->memberList.end(); i++) {
        *(int*)(&memInfo.addr.addr) = i->getid();
        *(short*)(&memInfo.addr.addr[4]) = i->getport();
        memInfo.heartbeat = i->getheartbeat();

        memberPkg->memInfo.push_back(memInfo);
    }

    // and the node itselve
    memInfo.addr = memberNode->addr;
    memInfo.heartbeat = memberNode->heartbeat;
    memberPkg->memInfo.push_back(memInfo);

    memberPkg->msgType = type;
    memberPkg->memNumber = memberNode->memberList.size() + 1;

    return memberPkg;
}

void MP1Node::updateMemberList(MemberInfo *memInfo)
{
    // if a member in vector is not in the list
    // update this member
    // if an item is in the list, update {heartbeat, timestamp}
    int id = *(int*)(&memInfo->addr.addr);
    short port = *(short*)(&memInfo->addr.addr[4]);
    long heartbeat = memInfo->heartbeat;
    long timestamp = par->getcurrtime();

    // this the node itself, ignore
    if (memberNode->addr == memInfo->addr) {
        return;
    }

    vector<MemberListEntry>::iterator i;
    for (i=(memberNode->memberList).begin(); i!= (memberNode->memberList).end(); i++) {
        if (i->id == id) {
            // if this node is on list, and newer heartbeat, exit
            if (i->heartbeat >= heartbeat)
                return;

            // if this node has older heartbeat, update
            if (i->heartbeat < heartbeat) {
                i->setheartbeat(heartbeat);
                i->settimestamp(timestamp);
                i->bFailed = false;
                return;
            }
        }
    }
    // else, adding new node
    MemberListEntry *newMember = new MemberListEntry;
    // int id = *(int*)(&memInfo->addr.addr[4]);
    (*newMember).setid(id);
    (*newMember).setport(port);
    (*newMember).setheartbeat(heartbeat);
    (*newMember).settimestamp(timestamp);
    (*newMember).bFailed = false;
    memberNode->memberList.push_back(*newMember);

    // and loging
    log->logNodeAdd(&memberNode->addr, &memInfo->addr);

    delete(newMember);
}

void MP1Node::verboseMemberList(void)
{
#if VERBOSE    
    printf("current membership list \n");
    printf("------------------------\n");
    vector<MemberListEntry>::const_iterator i;
    for (i=memberNode->memberList.begin(); i!= memberNode->memberList.end(); i++) {
        int id = i->id;
        long heartbeat = i->heartbeat;
        long timestamp = i->timestamp;

        printf("Node ID %d, heartbeat %ld, timestamp %ld \n",  
            id, heartbeat, timestamp);
    }
    printf("------------------------\n");
#else
#endif    
}
void serializeMemberMessage(MemberPkg &memberPkg, char **msg, int *len)
{
    // char *msg;
    size_t msgsize = sizeof(memberPkg.msgType) + memberPkg.memNumber + memberPkg.memInfo.size()*sizeof(MemberInfo) + 1;
    *msg = (char*)malloc(msgsize * sizeof(char));

    int pos = 0;
    vector<MemberInfo>::const_iterator i;

    memcpy(*msg+pos, (char*)&(memberPkg.msgType), sizeof(memberPkg.msgType)*sizeof(char));
    pos += sizeof(memberPkg.msgType) * sizeof(char);
    memcpy(*msg+pos, (char*)&(memberPkg.memNumber), sizeof(memberPkg.memNumber)*sizeof(char));
    pos += sizeof(memberPkg.memNumber) * sizeof(char);
    for (i=memberPkg.memInfo.begin(); i!= memberPkg.memInfo.end(); i++) {
        memcpy(*msg+pos, (char*)&(i->addr.addr), sizeof(i->addr.addr)*sizeof(char));
        pos += sizeof(i->addr.addr) * sizeof(char);
        memcpy(*msg+pos, (char*)&(i->heartbeat), sizeof(i->heartbeat)*sizeof(char));
        pos += sizeof(i->heartbeat) * sizeof(char);
    }

    *len = pos;
}

MemberPkg *parseMemberMessage(char *msg, int len)
{
    MemberPkg *memberPkg = new MemberPkg;

    int pos = 0;
    memcpy((char*)&(memberPkg->msgType), msg+pos, sizeof(memberPkg->msgType)*sizeof(char));
    pos += sizeof(memberPkg->memNumber) * sizeof(char);
    memcpy((char*)&(memberPkg->memNumber), msg+pos, sizeof(memberPkg->memNumber)*sizeof(char));
    pos += sizeof(memberPkg->memNumber) * sizeof(char);

    // printf("parse: Type: %d, member number: %d\n", memberPkg->msgType, memberPkg->memNumber);

    // todo: unsecure, not check for len
    MemberInfo memInfo;
    for (int i=0; i<(memberPkg->memNumber); i++) {
        memcpy((char *)&(memInfo.addr.addr), msg+pos, sizeof(memInfo.addr.addr)*sizeof(char));
        pos+=sizeof(memInfo.addr.addr)*sizeof(char);
        memcpy((char *)&(memInfo.heartbeat), msg+pos, sizeof(memInfo.heartbeat)*sizeof(char));
        pos+=sizeof(memInfo.heartbeat)*sizeof(char);
        memberPkg->memInfo.push_back(memInfo);
        // printf("parse: Node %d.%d.%d.%d:%d, heartbeat %ld\n",  
        //     memInfo.addr.addr[0], memInfo.addr.addr[1], memInfo.addr.addr[2], memInfo.addr.addr[3], *(short*)&memInfo.addr.addr[4],
        //     memInfo.heartbeat);
    }
    return memberPkg;
}

void verbosePkg(MemberPkg &memberPkg)
{
#if VERBOSE
    vector<MemberInfo>::const_iterator i;
    for (i=memberPkg.memInfo.begin(); i!= memberPkg.memInfo.end(); i++) {
        printf("Node %d.%d.%d.%d:%d, heartbeat %ld. ",  
            i->addr.addr[0], i->addr.addr[1], i->addr.addr[2], i->addr.addr[3], *(short*)&i->addr.addr[4],
            i->heartbeat);
    }
    printf("Msg type = %d, mem = %d\n", memberPkg.msgType, memberPkg.memNumber);
#else
#endif
}